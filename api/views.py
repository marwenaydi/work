from rest_framework.decorators import api_view
from rest_framework.response import Response
import random
import datetime


# Create your views here.
@api_view()
def reservation(request):
    start_date = datetime.date.today()
    end_date = start_date + datetime.timedelta(days=random.randint(1,5))
    res=list()
    for i in range(10):
        newguests = list ()
        nbrguest=random.randint(1,5)
        nbrroom=random.randint(1,nbrguest)
        newrooms = list ()
        totals = random.randint(100,300)
        guest={
        "guest_id_pms": "da15c5dc-7411-47ff-a9ed-ad6c00e28b90",
        "title": "",
        "language": "EN",
        "first_name": "",
        "last_name": "Groupe Cimalpes 04-05 Octobre 2021",
        "email": "",
        "phone": "",
        "loyalty_code": "",
        "address1": "",
        "address2": "",
        "gender": "",
        "city": "",
        "passport_number": "",
        "passport_expire_date": None,
        "passport_issuance_contry_code": "ND",
        "passport_issuance_date": None,
        "state": "",
        "country": "ND",
        "nationality": "ND",
        "zip_code": "",
        "birthday": None,
        "document_type": None,
        "birthday_place": "",
        "note": "",
        "is_main_guest": True,
        "reservation_guest_data": {
            "start_date": start_date.strftime("%Y-%d-%m"),
            "end_date": end_date.strftime("%Y-%d-%m")
        },
        "reservation_id_pms": "550ca1c1-6ca3-4404-8340-ad6c00e30661"
        }
        for seq in range(nbrguest):
            newguests.append(guest)
        for seq in range(nbrroom):
            room={
        "room_id_pms": "f66af656-f2bd-49c9-a50f-531c2ba3d45b",
        "room_name": "460",
        "room_number": "460",
        "other_rooms_number": [
            
        ],
        "room_type_pms": "a5742d5f-ade5-45e2-80e0-cfaa246e0932",
        "room_type": "room",
        "room_floor": "4",
        "room_building": "MONT-BLANC",
        "room_category_name": typeroom(),
        "reservation_room_data": {
            "start_date": "2021-10-04",
            "end_date": "2021-10-05",
            "guests": [
            "da15c5dc-7411-47ff-a9ed-ad6c00e28b90",
            "2d991f3b-b230-468d-a0f3-adb4009bb065",
            "212448c7-db54-465d-a679-adb4009bb069",
            "df32d5b7-8385-416d-bc16-adb4009bb06d",
            "0b1a7ce2-aa15-4b21-9928-adb4009bb070"
            ],
            "number_of_keys": 2,
            "guest_id_pms": "da15c5dc-7411-47ff-a9ed-ad6c00e28b90",
            "guest_first_name": "",
            "guest_last_name": "Groupe Cimalpes 04-05 Octobre 2021",
            "number_of_children": 0,
            "rates": [
            
            ],
            "balance": 0,
            "rate_id_pms": "62cf8b3f-7257-451f-8c21-ed80de8cd166",
            "is_available": False,
            "reservation_id_pms": "550ca1c1-6ca3-4404-8340-ad6c00e30661",
            "number_of_adults": 4,
            "room_total": 0
        
        }

        }
            newrooms.append(room)

        
        content={
               "customer": 48,
                "reservation_id_pms": "550ca1c1-6ca3-4404-8340-ad6c00e30661",
                "reservation_channel_number": None,
                "reservation_group_id_pms": "ed2b9d55-46d9-4471-a1e9-ad6c00e30661",
                "extra_reservation_code": "550ca1c1",
                "is_main_reservation": False,
                "start_date": "2021-10-04",
                "end_date": "2021-10-05",
                "arrival_time_estimated": "14:00:00",
                "departed_time_estimated": "09:00:00",
                "number_of_adults": 4,
                "number_of_keys": 2,
                "number_of_children": 0,
                "number_of_guests": 4,
                "number_of_night": 1,
                "travel_agency_id": None,
                "status": "Confirmed",
                "checked_in": False,
                "checked_out": False,
                "guests": newguests,
                "country_main_guest": "ND",
                "nationality_main_guest": "ND",
                "language_main_guets": "",
                "gender_main_guest": "",
                "first_name_main_guest": "",
                "last_name_main_guest": "Groupe Cimalpes 04-05 Octobre 2021",
                "email_main_guest": "",
                "title_main_guest": "",
                "main_guest_id": "da15c5dc-7411-47ff-a9ed-ad6c00e28b90",
                "number_of_rooms": 1,
                "rooms": newrooms,
                "main_room_number": "460",
                "total": totals,
                "balance": 0,
                "paid": 0,
                "invoice": "{}"
                }
        res.append(content)
    return Response(res)
def typeroom():
    room=['Chambre simple','Chambre double','king']
    x=random.randint(0,2)
    return room[x]


