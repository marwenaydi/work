from django.shortcuts import render
import requests
import json
from django.http.response import JsonResponse
from .models import reservation
import datetime

# Create your views here.
def stocke(request):
    responce=requests.get('http://127.0.0.1:5000/').json()
    for i in responce:
        y = json.dumps(i)
        reservations=reservation.objects.create(res=y)
        reservations.save()
    return JsonResponse(responce,safe=False)

def vip(request):
    obj = reservation.objects.filter(is_verified=False)
    users = []
    for k in obj:
        data = json.loads(k.res)
        start = data['start_date'].split("-")
        end = data['end_date'].split("-")
        start = datetime.date(year=int(start[0]),month=int(start[2]),day=int(start[1]))
        end = datetime.date(year=int(end[0]), month=int(end[2]), day=int(end[1]))
        who = end - start
        king = False
        for ptr in data['rooms']:
            if(ptr['room_category_name'] == 'king'):
                king = True
                break
        if(data['total'] >=200 and len(data['guests']) <= 2
        and who.days >= 2  and king
        ):
            users.append([data['customer'],data['first_name_main_guest']+data['last_name_main_guest'],data['start_date'],data['total']])
        k.is_verified = True
        k.save()
    return render(request,'consumer.html',{"users":users})



        
        
